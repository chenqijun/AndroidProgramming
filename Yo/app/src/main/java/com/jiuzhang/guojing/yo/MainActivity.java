package com.jiuzhang.guojing.yo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.app.Activity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Button;

@SuppressWarnings("ConstantConditions")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO find EditText by id
        final EditText editText = (EditText)findViewById(R.id.name);

        // TODO set up Buttons
        // find the Buttons you declared in the layout file by their ids
        // set up the click event listeners of the buttons
        // in each of the click event functions, display a toast that shows the text of the EditText


        View.OnClickListener myOnClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String message = editText.getText().toString();
                switch(v.getId()) {
                    case R.id.btn_1 : {
                        message = "YO " + message;
                        break;
                    }
                    case R.id.btn_2 : {
                        message = "MO " + message;
                        break;
                    }
                    case R.id.btn_3 : {
                        message = "YO YO " + message;
                        break;
                    }
                    case R.id.btn_4 : {
                        message = "OR " + message;
                        break;
                    }
                }

                Toast.makeText(getApplicationContext(), message,
                        Toast.LENGTH_SHORT).show();

                v.startAnimation(AnimationUtils.loadAnimation(v.getContext(), R.anim.text_anim));
            }
        };


        Button button1 = (Button)findViewById(R.id.btn_1); button1.setOnClickListener(myOnClickListener);
        Button button2 = (Button)findViewById(R.id.btn_2); button2.setOnClickListener(myOnClickListener);
        Button button3 = (Button)findViewById(R.id.btn_3); button3.setOnClickListener(myOnClickListener);
        Button button4 = (Button)findViewById(R.id.btn_4); button4.setOnClickListener(myOnClickListener);
    }
}

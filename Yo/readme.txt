Please check out the following branches in order
1. first_android_app: homework for building a simple Android program with step-by-step guide
2. YO: homework for building YO with step-by-step guide
3. master: complete code for YO app

Current branch: YO

Goals:
Build YO based on what you learned in first_android_app (YO 1.0)
Open "demo_YO" to see what YO looks like after you finish all TODOs AND the challenge.

In this branch we introduce new feature called "Challenge", which ask you to implement new functionality
without hints.
You need to finish these TODOs first before you "challenge" yourself.
These TODOs don't have orders, figure it out by yourself.

Challenge:
Figure out how to do animation, try searching this keyword: "android xml animation"

Life advice:
If you have any questions, try Google before asking someone else. You need PRACTICE how to get what you want.
If you can not find the answer, feel free to post your question in http://www.jiuzhang.com/qa or QQ group.
You can see the complete code (including code for Challenge) in "master" branch.
But keep in mind, don't check out the code in "master" branch until you have finished everything
all by yourself.